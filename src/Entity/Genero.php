<?php

namespace App\Entity;

use App\Repository\GeneroRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GeneroRepository::class)
 */
class Genero
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pelicula", mappedBy="genero")
     */
    private $pelicula;

    public function __construct()
    {
        $this->pelicula = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Pelicula[]
     */
    public function getPelicula(): Collection
    {
        return $this->pelicula;
    }

    public function addPelicula(Pelicula $pelicula): self
    {
        if (!$this->pelicula->contains($pelicula)) {
            $this->pelicula[] = $pelicula;
            $pelicula->setGenero($this);
        }

        return $this;
    }

    public function removePelicula(Pelicula $pelicula): self
    {
        if ($this->pelicula->removeElement($pelicula)) {
            // set the owning side to null (unless already changed)
            if ($pelicula->getGenero() === $this) {
                $pelicula->setGenero(null);
            }
        }

        return $this;
    }
}
