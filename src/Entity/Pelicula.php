<?php

namespace App\Entity;

use App\Repository\PeliculaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PeliculaRepository::class)
 */
class Pelicula
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=70)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sinopsis;

    /**
     * @ORM\Column(type="float")
     */
    private $precioUnitario;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaEstreno;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoPelicula", inversedBy="pelicula")
     */
    private $tipoPelicula;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Genero", inversedBy="pelicula")
     */
    private $genero;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetalleAlquiler", mappedBy="pelicula")
     */
    private $detalleAlquiler;

    public function __construct()
    {
        $this->detalleAlquiler = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getSinopsis(): ?string
    {
        return $this->sinopsis;
    }

    public function setSinopsis(?string $sinopsis): self
    {
        $this->sinopsis = $sinopsis;

        return $this;
    }

    public function getPrecioUnitario(): ?float
    {
        return $this->precioUnitario;
    }

    public function setPrecioUnitario(float $precioUnitario): self
    {
        $this->precioUnitario = $precioUnitario;

        return $this;
    }

    public function getFechaEstreno(): ?\DateTimeInterface
    {
        return $this->fechaEstreno;
    }

    public function setFechaEstreno(?\DateTimeInterface $fechaEstreno): self
    {
        $this->fechaEstreno = $fechaEstreno;

        return $this;
    }

    public function getTipoPelicula(): ?TipoPelicula
    {
        return $this->tipoPelicula;
    }

    public function setTipoPelicula(?TipoPelicula $tipoPelicula): self
    {
        $this->tipoPelicula = $tipoPelicula;

        return $this;
    }

    public function getGenero(): ?Genero
    {
        return $this->genero;
    }

    public function setGenero(?Genero $genero): self
    {
        $this->genero = $genero;

        return $this;
    }

    public function getDetalleAlquiler(): ?DetalleAlquiler
    {
        return $this->detalleAlquiler;
    }

    public function setDetalleAlquiler(?DetalleAlquiler $detalleAlquiler): self
    {
        $this->detalleAlquiler = $detalleAlquiler;

        return $this;
    }

    public function addDetalleAlquiler(DetalleAlquiler $detalleAlquiler): self
    {
        if (!$this->detalleAlquiler->contains($detalleAlquiler)) {
            $this->detalleAlquiler[] = $detalleAlquiler;
            $detalleAlquiler->setPelicula($this);
        }

        return $this;
    }

    public function removeDetalleAlquiler(DetalleAlquiler $detalleAlquiler): self
    {
        if ($this->detalleAlquiler->removeElement($detalleAlquiler)) {
            // set the owning side to null (unless already changed)
            if ($detalleAlquiler->getPelicula() === $this) {
                $detalleAlquiler->setPelicula(null);
            }
        }

        return $this;
    }
}
