<?php

namespace App\Entity;

use App\Repository\DetalleAlquilerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DetalleAlquilerRepository::class)
 */
class DetalleAlquiler
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $valor;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaAlquiler;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaRegreso;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Alquiler", inversedBy="detalleAlquiler")
    */
   private $alquiler;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pelicula", inversedBy="detalleAlquiler")
     */
    private $pelicula;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValor(): ?float
    {
        return $this->valor;
    }

    public function setValor(float $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getFechaAlquiler(): ?\DateTimeInterface
    {
        return $this->fechaAlquiler;
    }

    public function setFechaAlquiler(\DateTimeInterface $fechaAlquiler): self
    {
        $this->fechaAlquiler = $fechaAlquiler;

        return $this;
    }

    public function getFechaRegreso(): ?\DateTimeInterface
    {
        return $this->fechaRegreso;
    }

    public function setFechaRegreso(\DateTimeInterface $fechaRegreso): self
    {
        $this->fechaRegreso = $fechaRegreso;

        return $this;
    }

    public function getAlquiler(): ?Alquiler
    {
        return $this->alquiler;
    }

    public function setAlquiler(?Alquiler $alquiler): self
    {
        $this->alquiler = $alquiler;

        return $this;
    }

    public function getPelicula(): ?Pelicula
    {
        return $this->pelicula;
    }

    public function setPelicula(?Pelicula $pelicula): self
    {
        $this->pelicula = $pelicula;

        return $this;
    }
}
