<?php

namespace App\Entity;

use App\Repository\AlquilerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AlquilerRepository::class)
 */
class Alquiler
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $ValorTotal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cliente", inversedBy="alquiler")
     */
    private $cliente;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetalleAlquiler", mappedBy="alquiler")
     */
    private $detalleAlquiler;

    public function __construct()
    {
        $this->detalleAlquiler = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValorTotal(): ?float
    {
        return $this->ValorTotal;
    }

    public function setValorTotal(float $ValorTotal): self
    {
        $this->ValorTotal = $ValorTotal;

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * @return Collection|DetalleAlquiler[]
     */
    public function getDetalleAlquiler(): Collection
    {
        return $this->detalleAlquiler;
    }

    public function addDetalleAlquiler(DetalleAlquiler $detalleAlquiler): self
    {
        if (!$this->detalleAlquiler->contains($detalleAlquiler)) {
            $this->detalleAlquiler[] = $detalleAlquiler;
            $detalleAlquiler->setAlquiler($this);
        }

        return $this;
    }

    public function removeDetalleAlquiler(DetalleAlquiler $detalleAlquiler): self
    {
        if ($this->detalleAlquiler->removeElement($detalleAlquiler)) {
            // set the owning side to null (unless already changed)
            if ($detalleAlquiler->getAlquiler() === $this) {
                $detalleAlquiler->setAlquiler(null);
            }
        }

        return $this;
    }

}
