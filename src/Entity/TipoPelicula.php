<?php

namespace App\Entity;

use App\Repository\TipoPeliculaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TipoPeliculaRepository::class)
 */
class TipoPelicula
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $tipo;

    /**
     * @ORM\Column(type="float")
     */
    private $porcentajeIncremento;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $diasSinIncremento;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Pelicula", mappedBy="tipoPelicula")
     */
    private $pelicula;

    public function __construct()
    {
        $this->pelicula = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getPorcentajeIncremento(): ?float
    {
        return $this->porcentajeIncremento;
    }

    public function setPorcentajeIncremento(float $porcentajeIncremento): self
    {
        $this->porcentajeIncremento = $porcentajeIncremento;

        return $this;
    }

    public function getDiasSinIncremento(): ?int
    {
        return $this->diasSinIncremento;
    }

    public function setDiasSinIncremento(?int $diasSinIncremento): self
    {
        $this->diasSinIncremento = $diasSinIncremento;

        return $this;
    }

    /**
     * @return Collection|Pelicula[]
     */
    public function getPelicula(): Collection
    {
        return $this->pelicula;
    }

    public function addPelicula(Pelicula $pelicula): self
    {
        if (!$this->pelicula->contains($pelicula)) {
            $this->pelicula[] = $pelicula;
            $pelicula->setTipoPelicula($this);
        }

        return $this;
    }

    public function removePelicula(Pelicula $pelicula): self
    {
        if ($this->pelicula->removeElement($pelicula)) {
            // set the owning side to null (unless already changed)
            if ($pelicula->getTipoPelicula() === $this) {
                $pelicula->setTipoPelicula(null);
            }
        }

        return $this;
    }
}
