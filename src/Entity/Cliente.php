<?php

namespace App\Entity;

use App\Repository\ClienteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClienteRepository::class)
 */
class Cliente
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=70)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $identificacion;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $correo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Alquiler", mappedBy="cliente")
     */
    private $alquiler;

    public function __construct()
    {
        $this->alquiler = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getIdentificacion(): ?string
    {
        return $this->identificacion;
    }

    public function setIdentificacion(string $identificacion): self
    {
        $this->identificacion = $identificacion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(?string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * @return Collection|Alquiler[]
     */
    public function getAlquiler(): Collection
    {
        return $this->alquiler;
    }

    public function addAlquiler(Alquiler $alquiler): self
    {
        if (!$this->alquiler->contains($alquiler)) {
            $this->alquiler[] = $alquiler;
            $alquiler->setCliente($this);
        }

        return $this;
    }

    public function removeAlquiler(Alquiler $alquiler): self
    {
        if ($this->alquiler->removeElement($alquiler)) {
            // set the owning side to null (unless already changed)
            if ($alquiler->getCliente() === $this) {
                $alquiler->setCliente(null);
            }
        }

        return $this;
    }
}
