<?php

namespace App\Repository;

use App\Entity\TipoPelicula;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TipoPelicula|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoPelicula|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoPelicula[]    findAll()
 * @method TipoPelicula[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoPeliculaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoPelicula::class);
    }

    // /**
    //  * @return TipoPelicula[] Returns an array of TipoPelicula objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoPelicula
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
