<?php

namespace App\Repository;

use App\Entity\DetalleAlquiler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DetalleAlquiler|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetalleAlquiler|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetalleAlquiler[]    findAll()
 * @method DetalleAlquiler[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetalleAlquilerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetalleAlquiler::class);
    }

    // /**
    //  * @return DetalleAlquiler[] Returns an array of DetalleAlquiler objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetalleAlquiler
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
