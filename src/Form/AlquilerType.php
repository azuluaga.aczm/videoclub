<?php

namespace App\Form;

use App\Entity\Alquiler;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\Cliente;
use App\Form\DetalleAlquilerType;

class AlquilerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('cliente', EntityType::class, [
            'class'=> Cliente::class,
            'choice_label' =>'identificacion'   
        ] )
        ->add('ValorTotal')
        ->add('detalleAlquiler', CollectionType::class, [
            'entry_type' => DetalleAlquilerType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true,
            'by_reference' => false,
            'prototype' => 'detalleAlquiler'
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Alquiler::class,
        ]);
    }
}
