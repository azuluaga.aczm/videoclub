<?php

namespace App\Form;

use App\Entity\Pelicula;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\TipoPelicula;
use App\Entity\Genero;


class PeliculaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('sinopsis')
            ->add('precioUnitario')
            ->add('fechaEstreno')
            ->add('tipoPelicula', EntityType::class, [
                'class'=>TipoPelicula::class,
                'choice_label'=>'tipo',
            ])
            ->add('genero', EntityType::class, [
                'class'=>Genero::class,
                'choice_label'=>'nombre',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Pelicula::class,
        ]);
    }
}
