<?php

namespace App\Controller;

use App\Entity\DetalleAlquiler;
use App\Form\DetalleAlquilerType;
use App\Repository\DetalleAlquilerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/detalle/alquiler")
 */
class DetalleAlquilerController extends AbstractController
{
    /**
     * @Route("/", name="detalle_alquiler_index", methods={"GET"})
     */
    public function index(DetalleAlquilerRepository $detalleAlquilerRepository): Response
    {
        return $this->render('detalle_alquiler/index.html.twig', [
            'detalle_alquilers' => $detalleAlquilerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="detalle_alquiler_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $detalleAlquiler = new DetalleAlquiler();
        $form = $this->createForm(DetalleAlquilerType::class, $detalleAlquiler);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($detalleAlquiler);
            $entityManager->flush();

            return $this->redirectToRoute('detalle_alquiler_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('detalle_alquiler/new.html.twig', [
            'detalle_alquiler' => $detalleAlquiler,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="detalle_alquiler_show", methods={"GET"})
     */
    public function show(DetalleAlquiler $detalleAlquiler): Response
    {
        return $this->render('detalle_alquiler/show.html.twig', [
            'detalle_alquiler' => $detalleAlquiler,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="detalle_alquiler_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, DetalleAlquiler $detalleAlquiler, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DetalleAlquilerType::class, $detalleAlquiler);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('detalle_alquiler_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('detalle_alquiler/edit.html.twig', [
            'detalle_alquiler' => $detalleAlquiler,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="detalle_alquiler_delete", methods={"POST"})
     */
    public function delete(Request $request, DetalleAlquiler $detalleAlquiler, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$detalleAlquiler->getId(), $request->request->get('_token'))) {
            $entityManager->remove($detalleAlquiler);
            $entityManager->flush();
        }

        return $this->redirectToRoute('detalle_alquiler_index', [], Response::HTTP_SEE_OTHER);
    }
}
