<?php

namespace App\Controller;

use App\Entity\Pelicula;
use App\Form\PeliculaType;
use App\Repository\PeliculaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pelicula")
 */
class PeliculaController extends AbstractController
{
    /**
     * @Route("/", name="pelicula_index", methods={"GET"})
     */
    public function index(PeliculaRepository $peliculaRepository): Response
    {
        return $this->render('pelicula/index.html.twig', [
            'peliculas' => $peliculaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pelicula_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $pelicula = new Pelicula();
        $form = $this->createForm(PeliculaType::class, $pelicula);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($pelicula);
            $entityManager->flush();

            return $this->redirectToRoute('pelicula_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pelicula/new.html.twig', [
            'pelicula' => $pelicula,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="pelicula_show", methods={"GET"})
     */
    public function show(Pelicula $pelicula): Response
    {
        return $this->render('pelicula/show.html.twig', [
            'pelicula' => $pelicula,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pelicula_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Pelicula $pelicula, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PeliculaType::class, $pelicula);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('pelicula_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pelicula/edit.html.twig', [
            'pelicula' => $pelicula,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="pelicula_delete", methods={"POST"})
     */
    public function delete(Request $request, Pelicula $pelicula, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pelicula->getId(), $request->request->get('_token'))) {
            $entityManager->remove($pelicula);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pelicula_index', [], Response::HTTP_SEE_OTHER);
    }
}
