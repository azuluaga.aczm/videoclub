<?php

namespace App\Controller;

use App\Entity\Alquiler;
use App\Form\AlquilerType;
use App\Repository\AlquilerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/alquiler")
 */
class AlquilerController extends AbstractController
{
    /**
     * @Route("/", name="alquiler_index", methods={"GET"})
     */
    public function index(AlquilerRepository $alquilerRepository): Response
    {
        return $this->render('alquiler/index.html.twig', [
            'alquilers' => $alquilerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="alquiler_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $alquiler = new Alquiler();
        $form = $this->createForm(AlquilerType::class, $alquiler);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($alquiler);
            $entityManager->flush();

            return $this->redirectToRoute('alquiler_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('alquiler/new.html.twig', [
            'alquiler' => $alquiler,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="alquiler_show", methods={"GET"})
     */
    public function show(Alquiler $alquiler): Response
    {
        return $this->render('alquiler/show.html.twig', [
            'alquiler' => $alquiler,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="alquiler_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Alquiler $alquiler, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AlquilerType::class, $alquiler);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('alquiler_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('alquiler/edit.html.twig', [
            'alquiler' => $alquiler,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="alquiler_delete", methods={"POST"})
     */
    public function delete(Request $request, Alquiler $alquiler, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$alquiler->getId(), $request->request->get('_token'))) {
            $entityManager->remove($alquiler);
            $entityManager->flush();
        }

        return $this->redirectToRoute('alquiler_index', [], Response::HTTP_SEE_OTHER);
    }
}
